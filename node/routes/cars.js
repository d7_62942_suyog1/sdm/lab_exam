const express = require('express')

const router = express.Router()


router.get("/cars",(request,response) => {
     

    response.send({
                   car_id: 1,
                   Company_name: "Honda",
                   Car_model:  "suv",
                   price: 25
                        })
})

router.post("/cars",(request,response)=>{

    const {car_id,Company_name,Car_model,price} = request.query

    response.send(` ${car_id} ${Company_name} ${Car_model} ${price} `)
})

router.put("/cars",(request,response) =>{
   
    response.send({
        car_id: 1,
        Company_name: "Honda",
        Car_model:  "suv",
        price: 25
             })
       
    
})

router.delete("/car: car_id",(request,response)=>{

    response.send({
        car_id: 1,
        Company_name: "Honda",
        Car_model:  "suv",
        price: 25
             })
})

module.exports = router