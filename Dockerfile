FROM mysql

ENV MYSQL_ROOT_PASSWORD root 

ENV  MYSQL_DATABASE car

COPY car.sql /docker-entrypoint-initdb.d 

EXPOSE 3306